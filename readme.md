Debian-package-builder: Installing and configuring tools to build Debian packages
-------------------------------------------

These playbooks require Ansible 1.2.

This Ansible role installs dpkg-dev, quilt, mercurial-buildpackage, pbuilder,
git-buildpackage and svn-buildpackage in order to get the stack needed to
build Debian packages.

It also provides your preferred .hgrc and .gitconfig files on
the remote server, to let you feel at home.


